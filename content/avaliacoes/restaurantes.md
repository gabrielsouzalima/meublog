---
title: "Restaurantes"
date: 2022-09-26T08:23:24-03:00
draft: false
---

# Restaurantes favoritos

{{<line_break>}}

Abaixo, listarei meus restaurantes favoritos, espero que gostem!

{{<line_break>}}

## Pizzaria Bráz Elettrica
Todas as receitas são criações de Anthony Falco. Pizza guru e ex integrante de uma das principais pizzarias do Brooklyn de Nova York, Tony combinou sabores locais e de alta qualidade com a pegada urbana e descontraída de NY. Se tiver a oportunidade, peça a pizza de cogumelos!
 
{{<line_break>}}

## Mocotó
Seu nome foi dado pelos clientes do Sr. José de Almeida, que começou vendendo um caldo de mocotó especial nesse espaço há mais de 40 anos. Atualmente, o restaurante Mocotó é liderado pelo filho do Sr. José, o Chef Rodrigo Oliveira, que começou a ajudá-lo aos 13 anos de idade e que “por acidente”, em 2004, inventou o famoso dadinho de tapioca. Localizado na Zona Norte de São Paulo, o Mocotó está entre os 50 melhores restaurantes da América Latina pela 50 Best Restaurants. Seu diferencial? Comida sertaneja!

{{<line_break>}}

## A Figueira Rubaiyat
A dica é que você prove o carpaccio de filé mignon com rúcula como entrada e a tirita de picanha ou baby beef como prato principal acompanhado de um bom vinho tinto. E ao final, peça o pudim de leite e um cafezinho para fechar com chave de ouro.

{{<line_break>}}

## Bar e Lanches Estadão
Abertos 24hrs oferecendo os melhores sanduíches e pratos, em destaque a feijoada, servida a partir de terças feiras as 18hrs ate as 18hrs da Quarta feira, alem de sexta e sábado. Pratos do dia, saladas, e o Melhor Pernil de São Paulo...
