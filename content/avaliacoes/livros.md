---
title: "Livros"
date: 2022-09-26T08:23:24-03:00
draft: false
---

# Livros favoritos

{{<line_break>}}
Abaixo, listarei meus livros favoritos, espero que gostem!

{{<line_break>}}

## A última grande lição
Nessa história real, o jornalista Mitch Albom narra suas conversas com um antigo professor da faculdade, chamado Morrie Schwartz. 
20 anos depois de sequer terem se encontrado, Mitch teve a oportunidade de conversou algumas vezes com seu antigo professor. Mais precisamente, foram 14 encontros, sempre às terças-feiras, que ocorreram na casa de Schwartz. Nela, eles conversavam sobre filosofia, sobre religião, sobre a morte e outros temas. Os dois amigos não puderam continuar com os encontros, pois Morrie estava acometido por uma doença terminal, que degradava seu estado de saúde a cada semana. 
Após o falecimento de Morrie, o jornalista Mitch decidiu escrever um livro com todos os ensinamentos que aprendeu durante os 14 encontros.

{{<line_break>}}

## Morte e Vida Severina
Descobri este livro quando estava no ensino médio e era obrigado a o ler para ir bem nas provas de literatura. Anos depois, no entanto, decidi ler o livro de novo, sem a pressão de ter que decorar cada detalhe.
Na obra, João Cabral de Melo Neto nos traz poemas, que de tão bem criado parecem até mesmo uma canção. Nos textos, o autor narra as dificuldades da vida no sertão nordestino, marcado pelo calor, pela seca e, muitas vezes, pela falta de vida. O poema mais famoso é chamado "Morte e vida severina", e marca a chegada de um retirante, ao litoral de Recife. Ao chegar lá, o retirante começa a se questionar sobre continuar com seu sofrimento, marcado pela fome, pela pobreza e pela sede, ou pular "para fora de uma ponte e da vida". Após conversar com um morador local, chamado José, Severino, retirante, é convencido a continuar com sua luta diária, pois "a vida se renova a cada dia" e que vale a pena vivê-la mesmo que seja uma "vida severina". 

{{<line_break>}}

## O primeiro homem
Esta biografia, escrita por James R. Hansen, nos traz a vida de Neil Armstrong, o primeiro homem a pisar na Lua. No livro, o autor conta desde o começo da vida de Armstrong até seus últimos dias. Nele, aprendemos sobre seu prematuro interesse por aviões, que o levou a aprender a pilotar aviões na fazenda de seu avô, passando por seu período na faculdade, onde estudou engenharia aeronáutica, e sua participação como aviador na guerra da Coréia, onde se destacou. Após seu período no exército, Neil virou pilote de testes de aviões supersônicos, posto que o tornou um forte candidato para participar do programa da NASA para enviar homens ao espaço.
O autor também nos traz problemas pessoais de Neil, como o falecimento de sua filha e como isso o impactou profundamente, levando-o, inclusive, a deixar uma homenagem para ela na superfície lunar. 
O estilo de vida pacato de Neil foi um dos motivos que levou a NASA a o escolher para ser o 1° homem a pisar na Lua, e não Buzz Aldrin, já que Neil dificilmente se envolvia em polêmicas. Esse mesmo jeito pacato levou Neil a fugir da fama após concluir um dos maiores marcos da humanidade, e buscar o isolamente das câmeras e a fugir de entrevistas. Ao final de sua vida, Neil vivia como um homem simples, que amava a engenharia e que nunca buscou grande reconhecimento pelos seus feitos.