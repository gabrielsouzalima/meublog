---
title: "Sobre mim"
date: 2022-09-26T08:11:38-03:00
draft: false
---

## Quem sou eu?

{{<line_break>}}

Meu nome é Gabriel Souza Lima, tenho 21 anos e sou de São Paulo.
Atualmente, estou no 3° ano de Engenharia Mecatrônica na Universidade de São Paulo. 
No 6º semestre, os alunos devem criar um site estático usando Hugo, para a disciplina de PMR3304 - Sistemas de Informação. Por isso, decidi criar um blog no qual irei compartilhar 3 coisas que gosto de fazer, como: comer, passear e ler livros.
