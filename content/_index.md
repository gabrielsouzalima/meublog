---
title: "Início"
date: 2022-09-26T08:11:38-03:00
draft: false
---

# Resenhas da semana - 01/10/2022 
{{<line_break>}}
## Restaurante: Lanchonete Matilda  
Esta semana, visitei a Lanchonete Matilda. Criado pela chef Renata Vanzetto, são servidos pratos inspirados na cozinha vietnamita, algo incomum na capital paulista. A indicação foi so meu irmão, que é um fã da chef Renata e desejava conhecer o restaurante há meses.

Nós fomos num sábado à noite. A rua Bela Cintra fica bastante movimentada neste horário, devido à grande quantidade de bares e restaurantes que atraem não só os paulistas, mas também os estrangeiros. Ao chegar, achei o restaurante pequeno, com 5 mesas de metal e um balcão. Os atendentes são muitos atenciosos,  bem-humorados e estavam dispostos a explicar todos os pratos e ingredientes que geraram dúvidas.

De entrada, pedi o o guioza de vegetais *Kung Fu* que estava sensacional e, como principal, o lanche *Vietnamita Vegano*. Apesar de bastante picante, foi um dos melhores lanches que provei nos últimos tempos. Não pedimos sobremesa, mas haviam algumas opções bastante interessantes e que pareciam valer a pena.

Recomendo visitar a lanchonete à noite, pois a Rua Bela Cintra fica bastante agitada e descontraída. Após o comer, é possível conhecer as muitas oportunidades oferecidas pela Consolação. 
{{<line_break>}}
#### Preço: R$ 150,00 (4 pessoas)
#### Nota: 9/10
#### Endereço: Rua Bela Cintra 1541 - Pinheiros
{{<line_break>}}
## Livro da semana: O poder do infinito, Steven Strogatz
O último livro que li se chama "O poder do infinito", de Steven Strogatz. Apesar do nome sugerir uma obra de auto-ajuda, o livro se propõem a explicar ao leitor a importância do Cálculo Diferencial e Integral. 

O livro começa com um resumo da histório da matemática, apresentando alguns nomes importantes e outros esquecidos pelo tempo desde a Grécia Antiga. Na metade do livro, o autor começa a abordar o surgimento do cálculo diferencial e como o conceito de dividir o mundo em partes infinitas foi extremamente difícil de ser aceito no início. Após a "consolidação" do cálculo, o autor apresenta de forma extremamente natural teoremas famosos, como o Teorema Fundamental do Cálculo, que, pela primeira vez, deixou de ser uma ideia abstrata e passou a ser algo visual para mim. 

No final do livro, Strogatz nos mostra as infinitas aplicações do Cálculo em nossas vidas, desde a construção dos foguetes levou o homem à Lua até ao funcionamente de coquetéis que combatem doenças autoimunes.

O livro foi uma grata surpresa, já que eu estava com receio de ler uma obra cheia de termos matemáticos.
{{<line_break>}}
#### Nota: 8/10
#### Preço: R$ 54,00


